# This file is a template, and might need editing before it works on your project.
FROM python:3.6-alpine

RUN apk add --update py-pip

RUN pip install pytest
RUN pip install pytest-html

RUN mkdir /app
WORKDIR /app

COPY *.py .

CMD ["pytest", "--html=html/index.html"]
